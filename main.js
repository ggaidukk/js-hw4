let a = prompt("Введіть перше число");
let b = prompt("Введіть друге число");
while (
  a === null ||
  b === null ||
  isNaN(a) ||
  isNaN(b) ||
  a === "" ||
  b === ""
) {
  a = prompt("Введіть перше число");
  b = prompt("Введіть друге число");
}
let method = prompt("Введіть знак (+, -, *, /)");
function count(a, b, method) {
  switch (method) {
    case "+":
      return a + b;
    case "-":
      return a - b;
    case "*":
      return a * b;
    case "/":
      return a / b;
  }
}
console.log(
  `${Number(a)} ${method} ${Number(b)} = ${count(Number(a), Number(b), method)}`
);

//вирішила завдання підвищеної складності, але без умови "при цьому значенням 
//за замовчуванням для кожної зі змінних має бути введена інформація раніше", тому що не зрозуміла
